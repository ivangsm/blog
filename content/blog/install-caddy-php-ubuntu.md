---
title: "Instalar Caddy y PHP7 en Ubuntu"
date: 2017-04-04T10:17:53-05:00
draft: false

# post thumb
image: "images/post/caddy.jpg"

# meta description
description: "this is meta description"

# taxonomies
categories:
  - "Web Server"
tags:
  - "Caddy"
  - "Linux"
  - "Ubuntu"

# post type
type: "post"
---

# Instalar Caddy y PHP7 en Ubuntu

Caddy es un servidor WEB muy rapido y sencillo de usar y configurar, está escrito en [GO](https://www.golang.org "Golang") soporta HTTP/2 y es extensible mediante plugins, tambien puede generar certificados para poder servir mediante HTTPS automáticamente, ademas es de codigo abierto y multiplataforma.

### <span style="color:#308ED2"> Instalar Caddy </span>

Antes que nada, instalaremos ***curl*** en caso de no tenerlo instalado

```
sudo apt install curl
```

---

#### <span style="color:#308ED2">Después para instalar caddy usaremos:</span>

```
curl https://getcaddy.com | bash
```

El comando anterior debería instalar Caddy en */etc/local/bin/caddy* , este comando instala la version basica de Caddy, puedes obtener un comando mas completo desde su sitio web incluyendo los plugins que te parezcan utiles.

---

#### <span style="color:#308ED2">Permitir que Caddy use la red interna</span>

```
sudo setcap cap_net_bind_service=+ep /usr/local/bin/caddy
```

---

#### <span style="color:#308ED2">Crea los directorios que Caddy usará para guardar Caddyfile y SSL y da permisos sobre ellos</span>

```sh
sudo mkdir /etc/caddy
sudo chown -R root:www-data /etc/caddy
sudo mkdir /etc/ssl/caddy
sudo chown -R www-data:root /etc/ssl/caddy
sudo chmod 0770 /etc/ssl/caddy
```

---

#### <span style="color:#308ED2">Crea Caddyfile, que es el archivo de configuración para Caddy</span>

```
sudo nano /etc/caddy/Caddyfile
```

y escribe

```sh
tudominio.com {
	root /var/www
	tls tuusuario@tudominio.com

    fastcgi / 127.0.0.1:9000 php {
            env PATH /bin
    }
}
```

En caso de solo querer hacer pruebas locales reemplaza tudominio.com por localhost:2017 o cualquier puerto que elijas.

Presiona **CTRL+O** para guardar, presiona **ENTER** para aceptar y por último **CTRL+X** para salir del editor.

---

### <span style="color:#308ED2">Instalar PHP7</span>

```
sudo apt install php7.0-fpm php7.0-cli
```

---

<span style="color:#308ED2">Algunas configuraciones para PHP</span>

```sh
sudo sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php/7.0/fpm/php.ini
sudo sed -i "s/;date.timezone.*/date.timezone = UTC/" /etc/php/7.0/fpm/php.ini
sudo sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=1/" /etc/php/7.0/fpm/php.ini
sudo sed -i "s/upload_max_filesize = .*/upload_max_filesize = 200M/" /etc/php/7.0/fpm/php.ini
sudo sed -i "s/post_max_size = .*/post_max_size = 200M/" /etc/php/7.0/fpm/php.ini
```
---

<span style="color:#308ED2">Acepta la solicitud de PHP-FPM en un socket TCP en lugar de Unix

```
sudo nano /etc/php/7.0/fpm/pool.d/www.conf
```

Reemplaza la línea

```s
listen = /run/php/php7.0-fpm.sock
```

por

```s
listen = 127.0.0.1:9000
```

Presiona **CTRL+O** para guardar, presiona **ENTER** para aceptar y por último **CTRL+X** para salir del editor.

Para comprobar que se haya instalado correctamente utilizaremos el comando

```
php -v
```

y nos debería entregar una salida parecida a esta

```
PHP 7.0.15-0ubuntu0.16.04.4 (cli) ( NTS )
Copyright (c) 1997-2017 The PHP Group
Zend Engine v3.0.0, Copyright (c) 1998-2017 Zend Technologies
    with Zend OPcache v7.0.15-0ubuntu0.16.04.4, Copyright (c) 1999-2017, by Z
end Technologies
```

---
#### <span style="color:#308ED2">Creación de directorios</span>

Lo siguiente crea los directorios donde establecerás los archivos de tu sitio .html .php .js etc

```sh
sudo mkdir /var/www
sudo chown -R www-data:www-data /var/www
```

---

Para usar caddy solo basta poner el siguiente comando en caso de que tu servidor sea local o para pruebas.

```sh
caddy -conf="/etc/caddy/Caddyfile"
```

---

Si queremos usarlo como un servicio, es decir, que se inicie automáticamente con nuestro sistema introduciremos el siguiente comando.

```sh
sudo wget -O /etc/systemd/system/caddy.service https://raw.githubusercontent.com/mholt/caddy/master/dist/init/linux-systemd/caddy.service 
```

---

Y para habilitar e iniciar el servicio

```sh
sudo systemctl enable caddy.service
sudo systemctl start caddy.service
```

---

Con el servidor corriendo con cualquiera de las dos formas crearemos un archivo .php para verificar que todo funciona correctamente.

```sh
sudo nano /var/www/prueba.php
```

---

Y escribiremos en el:

```php
<?php
    phpinfo();
?>
```

---

Presiona **CTRL+O** para guardar, presiona **ENTER** para aceptar y por último **CTRL+X** para salir del editor.

Ahora abriremos nuestro navegador y escribiremos:

```
tudominio.com/prueba.php
```

o

```sh
localhost:2017/prueba.php

#(O el puerto que hayas utilizado)
``` 

después de hacer la prueba y verificar que todo funcione correctamente ya podemos eliminar el archivo de prueba

```sh
sudo rm /var/www/prueba.php
```

---

P.D. Cabe aclarar que para que funcione tu dominio y el habilitado de HTTPS debes tener abiertos el puerto 80 y el 443 en tu router y tu dominio tiene que apuntar a la IP de tu servidor.

---

Si tienes alguna duda, comentario o sugerencia, envia un mail a [admin@ivansalazar.org](mailto:admin@ivansalazar.org)